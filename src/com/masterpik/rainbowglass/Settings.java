package com.masterpik.rainbowglass;

import org.bukkit.Material;
import org.bukkit.inventory.ItemStack;
import org.bukkit.inventory.meta.ItemMeta;

public class Settings {

    public static String chatPrefix = "§8[§cRa§ein§abo§bwG§dla§6ss§8] §r";
    public static ItemStack rainbowItem = new ItemStack(Material.BLAZE_ROD, 1);

    public static void settingsInit() {
        ItemMeta itemMeta = rainbowItem.getItemMeta();
        itemMeta.setDisplayName(chatPrefix);
        rainbowItem.setItemMeta(itemMeta);
    }

}
