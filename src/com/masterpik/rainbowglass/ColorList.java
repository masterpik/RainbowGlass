package com.masterpik.rainbowglass;

import java.util.ArrayList;

public class ColorList {

    public static ArrayList<Byte> colorList;

    public static void colorInit() {

        colorList = new ArrayList<>();

        int bucle = 0;

        while (bucle <= 15) {
            colorList.add((byte) bucle);
            bucle ++;
        }

    }

}
