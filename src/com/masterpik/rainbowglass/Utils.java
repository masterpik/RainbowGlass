package com.masterpik.rainbowglass;

import org.bukkit.Bukkit;
import org.bukkit.Location;
import org.bukkit.entity.Player;

import java.util.ArrayList;

public class Utils {

    public static ArrayList<String> StringToArray(String string) {
        ArrayList<String> list = new ArrayList<String>();

        int bucle1 = 0;
        int bucle2 = 0;
        int count = 0;
        int bcl = 0;
        boolean okay1 = false;
        boolean finish = false;
        int calculsavant = 0;

        bcl = 0;
        while (!finish) {

            //Bukkit.getLogger().info("buclefinish, bcl : "+bcl);

            if (string.length() > bcl) {

                //Bukkit.getLogger().info("firstCond : "+string.charAt(bcl));
                //Bukkit.getLogger().info("secondCon : "+Integer.toString(count).charAt(0));

                if (string.charAt(bcl) == Integer.toString(count).charAt(0)
                        && string.charAt((bcl + 1)) == ':'
                        && string.charAt((bcl + 2)) == '(') {

                    bucle1 = bcl + 3;

                    okay1 = false;
                    while (!okay1) {

                        //Bukkit.getLogger().info("bucle1 while : "+bucle1);

                        if (string.charAt(bucle1) == ')') {
                            String fin = "";

                            calculsavant = 0;
                            calculsavant = (bucle1 - ((bcl + 3)));

                            bucle1 = bucle1 - calculsavant;

                            bucle2 = 0;

                            while (bucle2 < calculsavant) {

                                //Bukkit.getLogger().info("resultOfCalculSavant : "+calculsavant);

                                //Bukkit.getLogger().info("CharAt : "+(bucle1+bucle2));
                                fin = "" + fin + "" + string.charAt(bucle1 + bucle2);

                                bucle2++;
                            }
                            //Bukkit.getLogger().info("bucle2-1 : "+(bucle2));

                            list.add(fin);
                            //Bukkit.getLogger().info("La final : "+ fin);
                            okay1 = true;
                        }

                        bucle1++;
                    }
                    //Bukkit.getLogger().info("bucle1-1 : "+(bucle1-1));

                }
            }
            else {
                finish = true;
            }
            bcl = bcl+4+(bucle2);
            count++;
        }

        return list;
    }
    public static String ArrayToString(ArrayList<String> list) {
        String string = "";

        int bucle = 0;

        while (bucle < list.size()) {

            if (bucle == 0) {
                string = ""+bucle+":("+list.get(bucle)+")";
            }
            else {
                string = ""+string+""+bucle+":("+list.get(bucle)+")";
            }

            bucle++;
        }


        return string;
    }

    public static ArrayList<String> LocationToArray(Location location) {
        ArrayList<String> list = new ArrayList<String>();

        list.add(location.getWorld().getName());
        list.add(Integer.toString(location.getBlockX()));
        list.add(Integer.toString(location.getBlockY()));
        list.add(Integer.toString(location.getBlockZ()));

        return list;
    }
    public static Location ArrayToLocation(ArrayList<String> list) {
        return new Location(Bukkit.getWorld(list.get(0)), Integer.parseInt(list.get(1)), Integer.parseInt(list.get(2)), Integer.parseInt(list.get(3)));
    }

    public static void giveObject(Player player) {

    }

}
