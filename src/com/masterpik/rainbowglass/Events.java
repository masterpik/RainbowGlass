package com.masterpik.rainbowglass;

import com.masterpik.rainbowglass.confy.RainbowConfy;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.entity.Player;
import org.bukkit.event.EventHandler;
import org.bukkit.event.Listener;
import org.bukkit.event.block.Action;
import org.bukkit.event.player.PlayerInteractEvent;

import java.util.ArrayList;

public class Events implements Listener {

    @EventHandler
    public void onInteract(PlayerInteractEvent event) {
        Player player = event.getPlayer();
        Action action = event.getAction();
        Block block = event.getClickedBlock();

        if (player.getItemInHand().isSimilar(Settings.rainbowItem)){

            if (player.isOp()) {

                if (block.getType().equals(Material.GLASS) || block.getType().equals(Material.STAINED_GLASS)){

                    ArrayList<Block> list = RainbowConfy.getList();

                    if (action.equals(Action.RIGHT_CLICK_BLOCK)) {

                        if (list.contains(block)) {
                            player.sendMessage(Settings.chatPrefix + "§4Cette vitre est déjà enregistré");
                        } else {
                            RainbowConfy.addRainbow(block);
                            player.sendMessage(Settings.chatPrefix + "§2Nouvelle vitre enregistré");
                        }

                    } else if (action.equals(Action.LEFT_CLICK_BLOCK)) {

                        if (list.contains(block)) {
                            RainbowConfy.removeRainbow(block);
                            block.setType(Material.GLASS);
                            player.sendMessage(Settings.chatPrefix + "§2Vitre supprimé");
                        } else {
                            player.sendMessage(Settings.chatPrefix + "§4Cette vitre n'est pas enregistré");
                        }
                    }
                } else {
                    player.sendMessage(Settings.chatPrefix + "§4Vous devez cliquer sur du verre");
                }

            } else {
                player.sendMessage(Settings.chatPrefix + "§4Tu doit etre Op pour avoir le pouvoir");
            }
            event.setCancelled(true);
        }

    }


}
