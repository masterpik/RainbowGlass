package com.masterpik.rainbowglass.confy;

import org.bukkit.Bukkit;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;

public class GeneralConfy {

    public static void generalInit() {
        File file = new File(SettingsConfy.generalConfyPath);

        if (!file.exists()) {
            FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

            fileConfig.createSection("General");

            fileConfig.set("General.interval", 10);

            try {
                fileConfig.save(file);
                Bukkit.getLogger().info("Configuration File Created");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static int getInterval() {
        File file = new File(SettingsConfy.generalConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        return fileConfig.getInt("General.interval");
    }

}
