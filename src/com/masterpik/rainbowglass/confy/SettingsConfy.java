package com.masterpik.rainbowglass.confy;

public class SettingsConfy {

    public static final String generalConfyPath = "plugins/RainbowGlass/general.yml";
    public static final String rainbowConfyPath = "plugins/RainbowGlass/rainbow.yml";

}
