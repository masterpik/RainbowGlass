package com.masterpik.rainbowglass.confy;

import com.masterpik.rainbowglass.Utils;
import org.bukkit.Bukkit;
import org.bukkit.block.Block;
import org.bukkit.configuration.file.FileConfiguration;
import org.bukkit.configuration.file.YamlConfiguration;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;

public class RainbowConfy {

    public static void rainbowInit() {
        File file = new File(SettingsConfy.rainbowConfyPath);

        if (!file.exists()) {
            FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

            fileConfig.createSection("Rainbow");

            ArrayList<String> list = new ArrayList<String>();
            list.add("null");
            fileConfig.set("Rainbow.list", list);

            try {
                fileConfig.save(file);
                Bukkit.getLogger().info("Configuration File Created");
            } catch (IOException e1) {
                e1.printStackTrace();
            }
        }
    }

    public static void addRainbow(Block block) {
        File file = new File(SettingsConfy.rainbowConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        ArrayList<String> list = (ArrayList<String>) fileConfig.getList("Rainbow.list");

        if (list.contains("null")) {
            list.remove("null");
        }

        String locationToString = Utils.ArrayToString(Utils.LocationToArray(block.getLocation()));

        list.add(locationToString);

        fileConfig.set("Rainbow.list", list);

        try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}
    }

    public static void removeRainbow(Block block) {
        File file = new File(SettingsConfy.rainbowConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        ArrayList<String> list = (ArrayList<String>) fileConfig.getList("Rainbow.list");

        String toRemove = Utils.ArrayToString(Utils.LocationToArray(block.getLocation()));

        list.remove(toRemove);

        if (list.size() == 0) {
            list.add("null");
        }

        fileConfig.set("Rainbow.list", list);

        try {fileConfig.save(file);} catch (IOException e1) {e1.printStackTrace();}
    }

    public static ArrayList<Block> getList() {
        File file = new File(SettingsConfy.rainbowConfyPath);
        FileConfiguration fileConfig = YamlConfiguration.loadConfiguration(file);

        ArrayList<String> rainbowList = (ArrayList<String>) fileConfig.getList("Rainbow.list");
        ArrayList<Block> blockList = new ArrayList<Block>();


        if (rainbowList.size() != 0) {

            if (!rainbowList.contains("null")) {

                int bucle = 0;

                while (bucle < rainbowList.size()) {
                    blockList.add(Utils.ArrayToLocation(Utils.StringToArray(rainbowList.get(bucle))).getBlock());
                    bucle++;
                }

                return blockList;
            }
            else {
                return blockList;
            }
        }
        else {
            return blockList;
        }

    }

}
