package com.masterpik.rainbowglass;

import com.masterpik.rainbowglass.confy.GeneralConfy;
import org.bukkit.Bukkit;
import org.bukkit.command.Command;
import org.bukkit.command.CommandExecutor;
import org.bukkit.command.CommandSender;
import org.bukkit.entity.Player;

public class Commands implements CommandExecutor {
    @Override
    public boolean onCommand(CommandSender sender, Command command, String label, String[] args) {

        if (sender instanceof Player) {
            Player player = (Player) sender;

            if (player.isOp()) {

                if (command.getName().equalsIgnoreCase("rbg")) {
                    String arg = "null";

                    if (args.length == 1) {
                        arg = args[0];
                    }
                    else if (args.length == 0) {
                        arg = "take";
                    }
                    else {
                        arg = "null";
                    }

                    if (arg.equalsIgnoreCase("take")) {
                        player.getInventory().addItem(Settings.rainbowItem);
                        player.sendMessage(Settings.chatPrefix + "§2Clique sur une vitre avec le baton jaune pour la rendre multicolor");
                    }
                    else if (arg.equalsIgnoreCase("reload")) {
                        Main.timee.cancel();
                        player.sendMessage(Settings.chatPrefix + "§2Reload");
                        Main.timee = new Timer().runTaskTimer(Main.plugin, 0, GeneralConfy.getInterval());
                    }
                    else {
                        player.sendMessage(Settings.chatPrefix + "§2Tape /rbg pour avoir la magie en main");
                    }

                    return true;

                }

            }
            else {
                player.sendMessage(Settings.chatPrefix + "§4Tu doit etre Op pour avoir le pouvoir");
                return true;
            }

        }
        else {
            Bukkit.getLogger().info("Vous devez etre un joueur pour executer cette commande");
            return false;
        }


        return false;
    }
}
