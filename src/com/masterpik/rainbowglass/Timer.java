package com.masterpik.rainbowglass;

import com.masterpik.rainbowglass.confy.RainbowConfy;
import org.bukkit.Material;
import org.bukkit.block.Block;
import org.bukkit.scheduler.BukkitRunnable;

import java.util.ArrayList;
import java.util.Collections;

public class Timer extends BukkitRunnable {
    @Override
    public void run() {

        ArrayList<Block> list = RainbowConfy.getList();

        if (list != null) {
            int bucle = 0;

            while (bucle < list.size()) {

                list.get(bucle).setType(Material.STAINED_GLASS);

                Collections.shuffle(ColorList.colorList);

                list.get(bucle).setData(ColorList.colorList.get(0));

                bucle++;
            }
        }

    }
}
