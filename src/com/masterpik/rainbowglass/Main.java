package com.masterpik.rainbowglass;

import com.masterpik.rainbowglass.confy.GeneralConfy;
import com.masterpik.rainbowglass.confy.RainbowConfy;
import org.bukkit.Bukkit;
import org.bukkit.plugin.Plugin;
import org.bukkit.plugin.java.JavaPlugin;
import org.bukkit.scheduler.BukkitRunnable;
import org.bukkit.scheduler.BukkitTask;

public class Main extends JavaPlugin {

    public static BukkitTask timee;
    public static Plugin plugin;

    public void onEnable() {

        plugin = this;

        Bukkit.getPluginManager().registerEvents(new Events(), this);
        getCommand("rbg").setExecutor(new Commands());

        ColorList.colorInit();
        Settings.settingsInit();

        GeneralConfy.generalInit();
        RainbowConfy.rainbowInit();

        timee = new Timer().runTaskTimer(this, 0, GeneralConfy.getInterval());

    }

    public void onDisable() {
        timee.cancel();
    }

}
